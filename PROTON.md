<!--Only include games that users have verified to work in VR. Some VR games
are "VR Supported", as opposed to VR Only, and most people testing these games 
will not be able to test VR. SpaceEngine, for example, needs VR verified.-->

This list contains many VR titles which are *not* whitelisted or native (so you
will not get support from Valve or the game developers) but do have a Gold or
Platinum rating on ProtonDB.

"Gold" or "Platinum" are reflective of how many good ratings there are versus
poor ratings overall. "Platinum" is the best, and means the software is most
likely to work. Regardless, tweaks may be required, and updates may change how 
well it works with Proton, so be sure to read the reviews on ProtonDB.

* [Accounting+] - [Platinum][Accounting+ PDB]
* [A Fisherman's Tale] - [Gold][A Fisherman's Tale PDB]
* [A-Tech Cybernetic VR][ATCVR] - [Platinum][ATCVR PDB]
* [Aircar] - [Platinum][Aircar PDB]
* [Anyland] - [Platinum][Anyland PDB]
* [Arizona Sunshine] - [Gold][Arizona Sunshine PDB]
* [Assetto Corsa] - [Gold][Assetto Corsa PDB]* - [Needs alt launcher](#16)
* [Assetto Corsa Competizione][ACC] - [Gold][ACC PDB]* - [Needs custom Proton][ACCP]
* [Blade & Sorcery][B&S] - [Gold][B&S PDB]
* [Boneworks] - [Gold][Boneworks PDB]
* [Borderlands 2 VR] - [Platinum][Borderlands 2 VR PDB]
* [BOXVR] - [Platinum][BOXVR PDB]
* [Budget Cuts] - [Gold][Budget Cuts PDB]
* [COMPOUND] - [Gold][COMPOUND PDB]
* [CUSTOM ORDER MAID 3D2 It's a Night Magic][COM3D2] - [Gold][COM3D2 PDB]*
* [Down the Rabbit Hole] - [Platinum][DtRH PDB]
* [Duck Season] - [Gold][Duck Season PDB]
* [Elite Dangerous] - [Gold][ED PDB]*
* [Fallout 4 VR] - [Gold][FO4 PDB]
* [Five Nights at Freddy's: Help Wanted][FNAF:HW] - [Gold][FNAF:HW PDB]
* [FORM] - [Platinum][FORM PDB]
* [Gal*Gun VR] - [Gold][Gal*Gun VR PDB]
* [GORN] - [Gold][GORN PDB]
* [Hot Dogs, Horseshoes & Hand Grenades][H3VR] - [Gold][H3VR PDB]
* [I Expect You To Die][IEYTD] - [Gold][IEYTD PDB]
* [In Death] - [Platinum][In Death PDB]
* [Jet Island] - [Platinum][Jet Island PDB]
* [Job Simulator] - [Gold][Job Simulator PDB]
* [Karnage Chronicles] - [Platinum][Karnage Chronicles PDB]
* [Lazerbait] - [Platinum][Lazerbait PDB]
* [Mini Motor Racing X] - [Platinum][Mini Motor Racing X PDB]
* [Moss] - [Platinum][Moss PDB]
* [No Man's Sky] - [Gold][No Man's Sky PDB]* - 
  [Do not use experimental branch](#15)
* [OhShape] - [Platinum][OhShape PDB]
* [Operation Warcade VR] - [Gold][Operation Warcade VR PDB]
* [OrbusVR: Reborn] - [Gold][OrbusVR: Reborn PDB]
* [Payday 2] - [Platinum][Payday 2 PDB]*
* [Phasmophobia] - [Gold][Phasmophobia PDB]*
* [Pistol Whip] - [Gold][Pistol Whip PDB]
* [Project CARS 2] - [Platinum][Project CARS 2 PDB]*
* [Racket: Nx] - [Gold][Racket: Nx PDB]
* [Red Matter] - [Gold][Red Matter PDB]
* [Rick and Morty: Virtual Rick-ality][RaM:VR] - [Gold][RaM:VR PDB]
* [Space Pirate Trainer][SPT] - [Platinum][SPT PDB]
* [Sprint Vector] - [Gold][Sprint Vector PDB]
* [Star Trek: Bridge Crew] - [Gold][ST:BC PDB]*
* [SUPERHOT VR] - [Platinum][SUPERHOT VR PDB]
* [Synth Riders] - [Gold][Synth Riders PDB]
* [Tabletop Simulator] - [Platinum][Tabletop Simulator PDB]*
* [Tilt Brush] - [Gold][Tilt Brush PDB]
* [TO THE TOP] - [Gold][TO THE TOP PDB]
* [The Elder Scrolls V: Skyrim VR][TESV:SVR] - [Gold][TESV:SVR PDB]
* [The Room VR: A Dark Matter][TRVR:ADM] - [Gold][TRVR:ADM PDB]
* [The Thrill of the Fight - VR Boxing][TTotF] - [Gold][TTotF PDB]
* [The Walking Dead: Saints & Sinners][TWDS&S] - [Gold][TWDS&S PDB]
* [Thumper] - [Platinum][Thumper PDB]
* [Transpose] - [Platinum][Transpose PDB]
* [Vacation Simulator] - [Gold][Vacation Simulator PDB]
* [Vox Machinae] - [Platinum][Vox Machinae PDB]
* [VRChat] - [Gold][VRChat PDB]*
* [VR Paradise (sexual content)][VR Paradise] - [Platinum][VR Paradise PDB]
* [VR The Diner Duo] - [Platinum][VR The Diner Duo PDB]
* [VTOL VR] - [Gold][VTOL VR PDB]
* [Westworld Awakening] - [Gold][Westworld Awakening PDB]
* [Windlands] - [Platinum][Windlands PDB]
* [Zero Caliber VR] - [Gold][Zero Caliber VR PDB]
<!--* [Until You Fall][UYF] - [Platinum][UYF PDB]-->

**VR Supported* title, not VR Only. Most reviews likely do not reflect VR 
performance.

<!-- Link URLs -->

[Accounting+]: https://store.steampowered.com/app/927270
  [Accounting+ PDB]: https://www.protondb.com/app/927270
[A Fisherman's Tale]: https://store.steampowered.com/app/559330
  [A Fisherman's Tale PDB]: https://www.protondb.com/app/559330
[ATCVR]: https://store.steampowered.com/app/578210
  [ATCVR PDB]: https://www.protondb.com/app/578210
[Aircar]: https://store.steampowered.com/app/1073390
  [Aircar PDB]: https://www.protondb.com/app/1073390
[Anyland]: https://store.steampowered.com/app/505700
  [Anyland PDB]: https://www.protondb.com/app/505700
[Arizona Sunshine]: https://store.steampowered.com/app/342180
  [Arizona Sunshine PDB]: https://www.protondb.com/app/342180
[Assetto Corsa]: https://store.steampowered.com/app/244210
  [Assetto Corsa PDB]: https://www.protondb.com/app/244210
[ACC]: https://store.steampowered.com/app/805550
  [ACC PDB]: https://www.protondb.com/app/805550
  [ACCP]: https://github.com/ValveSoftware/Proton/pull/4163
[B&S]: https://store.steampowered.com/app/629730
  [B&S PDB]: https://www.protondb.com/app/629730
[Boneworks]: https://store.steampowered.com/app/823500
  [Boneworks PDB]: https://www.protondb.com/app/823500
[Borderlands 2 VR]: https://store.steampowered.com/app/991260
  [Borderlands 2 VR PDB]: https://www.protondb.com/app/991260
[BOXVR]: https://store.steampowered.com/app/641960
  [BOXVR PDB]: https://www.protondb.com/app/641960
[Budget Cuts]: https://store.steampowered.com/app/400940
  [Budget Cuts PDB]: https://www.protondb.com/app/400940
[COMPOUND]: https://store.steampowered.com/app/615120
  [COMPOUND PDB]: https://www.protondb.com/app/615120
[COM3D2]: https://store.steampowered.com/app/1097580
  [COM3D2 PDB]: https://www.protondb.com/app/1097580
[cyubeVR]: https://store.steampowered.com/app/619500
  [cyubeVR PDB]: https://www.protondb.com/app/619500
[Duck Season]: https://store.steampowered.com/app/503580
  [Duck Season PDB]: https://www.protondb.com/app/503580
[Down the Rabbit Hole]: https://store.steampowered.com/app/1215270
  [DtRH PDB]: https://www.protondb.com/app/1215270
[Elite Dangerous]: https://store.steampowered.com/app/359320
  [ED PDB]: https://www.protondb.com/app/359320
[Fallout 4 VR]: https://store.steampowered.com/app/611660
  [FO4 PDB]: https://www.protondb.com/app/611660
[FNAF:HW]: https://store.steampowered.com/app/732690
  [FNAF:HW PDB]: https://www.protondb.com/app/732690
[FORM]: https://store.steampowered.com/app/408520
  [FORM PDB]: https://www.protondb.com/app/408520
[Gal*Gun VR]: https://store.steampowered.com/app/678520
  [Gal*Gun VR PDB]: https://www.protondb.com/app/678520
[GORN]: https://store.steampowered.com/app/578620
  [GORN PDB]: https://www.protondb.com/app/578620
[H3VR]: https://store.steampowered.com/app/450540
  [H3VR PDB]: https://www.protondb.com/app/450540
[IEYTD]: https://store.steampowered.com/app/587430
  [IEYTD PDB]: https://www.protondb.com/app/587430
[In Death]: https://store.steampowered.com/app/605450
  [In Death PDB]: https://www.protondb.com/app/605450
[Jet Island]: https://store.steampowered.com/app/587220
  [Jet Island PDB]: https://www.protondb.com/app/587220
[Job Simulator]: https://store.steampowered.com/app/448280
  [Job Simulator PDB]: https://www.protondb.com/app/448280
[Karnage Chronicles]: https://store.steampowered.com/app/611160
  [Karnage Chronicles PDB]: https://www.protondb.com/app/611160
[Lazerbait]: https://store.steampowered.com/app/529150
  [Lazerbait PDB]: https://www.protondb.com/app/529150
[MGI:P]: https://store.steampowered.com/app/943700
  [MGI:P PDB]: https://www.protondb.com/app/943700
[Mini Motor Racing X]: https://store.steampowered.com/1303990
  [Mini Motor Racing X PDB]: https://www.protondb.com/app/1303990
[Moss]: https://store.steampowered.com/app/846470
  [Moss PDB]: https://www.protondb.com/app/846470
[No Man's Sky]: https://store.steampowered.com/app/275850
  [No Man's Sky PDB]: https://www.protondb.com/app/275850
[OhShape]: https://store.steampowered.com/app/1098100
  [OhShape PDB]: https://www.protondb.com/app/1098100
[Operation Warcade VR]: https://store.steampowered.com/app/639270
  [Operation Warcade VR PDB]: https://www.protondb.com/app/639270
[OrbusVR: Reborn]: https://store.steampowered.com/app/746930
  [OrbusVR: Reborn PDB]: https://www.protondb.com/app/746930
[Phasmophobia]: https://store.steampowered.com/app/739630
  [Phasmophobia PDB]: https://www.protondb.com/app/739630
[Payday 2]: https://store.steampowered.com/app/218620
  [Payday 2 PDB]: https://www.protondb.com/app/218620
[Pistol Whip]: https://store.steampowered.com/app/1079800
  [Pistol Whip PDB]: https://www.protondb.com/app/1079800
[Project CARS 2]: https://store.steampowered.com/app/378860
  [Project CARS 2 PDB]: https://www.protondb.com/app/378860
[Racket: Nx]: https://store.steampowered.com/app/428080
  [Racket: Nx PDB]: https://www.protondb.com/app/428080
[Red Matter]: https://store.steampowered.com/app/966680
  [Red Matter PDB]: https://www.protondb.com/app/966680
[RaM:VR]: https://store.steampowered.com/app/469610
  [RaM:VR PDB]: https://www.protondb.com/app/469610
[SPT]: https://store.steampowered.com/app/418650
  [SPT PDB]: https://www.protondb.com/app/418650
[Sprint Vector]: https://store.steampowered.com/app/590690
  [Sprint Vector PDB]: https://www.protondb.com/app/590690
[Star Trek: Bridge Crew]: https://store.steampowered.com/app/527100
  [ST:BC PDB]: https://www.protondb.com/app/527100
[SUPERHOT VR]: https://store.steampowered.com/app/617830
  [SUPERHOT VR PDB]: https://www.protondb.com/app/617830
[Synth Riders]: https://store.steampowered.com/app/885000
  [Synth Riders PDB]: https://www.protondb.com/app/885000
[Tabletop Simulator]: https://store.steampowered.com/app/286160
  [Tabletop Simulator PDB]: https://www.protondb.com/app/286160
[TESV:SVR]: https://store.steampowered.com/app/611670
  [TESV:SVR PDB]: https://www.protondb.com/app/611670
[Tilt Brush]: https://store.steampowered.com/app/327140
  [Tilt Brush PDB]: https://www.protondb.com/app/327140
[TO THE TOP]: https://store.steampowered.com/app/509250
  [TO THE TOP PDB]: https://www.protondb.com/app/509250
[TRVR:ADM]: https://store.steampowered.com/app/1104380
  [TRVR:ADM PDB]: https://www.protondb.com/app/1104380
[TTotF]: https://store.steampowered.com/app/494150
  [TTotF PDB]: https://www.protondb.com/app/494150
[TWDS&S]: https://store.steampowered.com/app/916840
  [TWDS&S PDB]: https://www.protondb.com/app/916840
[Thumper]: https://store.steampowered.com/app/356400
  [Thumper PDB]: https://www.protondb.com/app/356400
[Transpose]: https://store.steampowered.com/app/835950
  [Transpose PDB]: https://www.protondb.com/app/835950
[UYF]: https://store.steampowered.com/app/858260
  [UYF PDB]: https://www.protondb.com/app/858260
[Vacation Simulator]: https://store.steampowered.com/app/726830
  [Vacation Simulator PDB]: https://www.protondb.com/app/726830
[Vox Machinae]: https://store.steampowered.com/app/334540
  [Vox Machinae PDB]: https://www.protondb.com/app/334540
[VRChat]: https://store.steampowered.com/app/438100
  [VRChat PDB]: https://www.protondb.com/app/438100
[VR Paradise]: https://store.steampowered.com/app/896890
  [VR Paradise PDB]: https://www.protondb.com/app/896890
[VR The Diner Duo]: https://store.steampowered.com/app/530120
  [VR The Diner Duo PDB]: https://www.protondb.com/app/530120
[VTOL VR]: https://store.steampowered.com/app/667970
  [VTOL VR PDB]: https://www.protondb.com/app/667970
[Westworld Awakening]: https://store.steampowered.com/app/1133320
  [Westworld Awakening PDB]: https://www.protondb.com/app/1133320
[Windlands]: https://store.steampowered.com/app/428370
  [Windlands PDB]: https://www.protondb.com/app/428370
[Zero Caliber VR]: https://store.steampowered.com/app/877200
  [Zero Caliber VR PDB]: https://www.protondb.com/app/877200
