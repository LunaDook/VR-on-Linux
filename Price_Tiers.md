Original thread:

https://www.reddit.com/r/vive_vr/comments/kqjgzp/last_day_of_the_steam_winter_sale_heres_my/

Adding Linux compatibility notes.

# Bargain Price Tier

|**Game**                                                                                             |**Linux?**|**Details**                                                                                     |
|-----------------------------------------------------------------------------------------------------|----------|------------------------------------------------------------------------------------------------|
|[**PAYDAY 2**](https://store.steampowered.com/app/218620/)                                           |Yes       |[Proton](https://www.protondb.com/app/218620)                                                   |
|[**Half-Life 1**](https://store.steampowered.com/app/70/HalfLife/)                                   |--        |[Newer SDK too much work for dev](https://github.com/maxvollmer/Half-Life-VR/issues/2)          |
|[**Half-Life 2**](https://store.steampowered.com/app/220/HalfLife_2/)                                |--        |[vrmod doesn’t support Linux](https://steamcommunity.com/sharedfiles/filedetails/?id=1678408548)|
|[**Naked Sun**](https://store.steampowered.com/app/762490/Naked_Sun/)                                |--        |[ProtonDB](https://www.protondb.com/app/762490)                                                 |
|[**Scanner Sombre**](https://store.steampowered.com/app/475190/Scanner_Sombre/)                      |--        |[ProtonDB](https://www.protondb.com/app/475190)                                                 |
|[**Steady**](https://store.steampowered.com/app/986670/Steady/)                                      |?         |No tests yet                                                                                    |
|[**Windlands**](https://store.steampowered.com/app/428370/Windlands/)                                |Yes       |[Proton](https://www.protondb.com/app/428370)                                                   |
|[**Evolution VR**](https://store.steampowered.com/app/549760/Evolution_VR/)                          |?         |No tests yet                                                                                    |
|[**PROZE: Enlightenment**](https://store.steampowered.com/app/924250/PROZE_Enlightenment/)           |?         |No tests yet                                                                                    |
|[**Evil Robot Traffic Jam HD**](https://store.steampowered.com/app/404390/Evil_Robot_Traffic_Jam_HD/)|?         |No tests yet                                                                                    |
|[**Interkosmos**](https://store.steampowered.com/app/579110/Interkosmos/)                            |--        |[Poor performance](https://www.protondb.com/app/579110)                                         |
|[**Neonwall**](https://store.steampowered.com/app/787650/Neonwall/)                                  |--        |[ProtonDB](https://www.protondb.com/app/787650)                                                 |
|[**Spuds Unearthed**](https://store.steampowered.com/app/909570/Spuds_Unearthed/)                    |?         |No tests yet                                                                                    |
|[**Squishies**](https://store.steampowered.com/app/593060/Squishies/)                                |?         |No tests yet                                                                                    |
|[**Zooma VR**](https://store.steampowered.com/app/1214410/Zooma_VR/)                                 |?         |No tests yet                                                                                    |
|[**Obstruction : VR**](https://store.steampowered.com/app/918060/Obstruction__VR/)                   |?         |No tests yet                                                                                    |
|[**Fingers: Mini Games**](https://store.steampowered.com/app/1348270/Fingers_Mini_Games/)            |?         |No tests yet                                                                                    |
|[**Chroma Lab**](https://store.steampowered.com/app/587470/Chroma_Lab/)                              |--        |[ProtonDB](https://www.protondb.com/app/587470)                                                 |
|[**Drone Hero**](https://store.steampowered.com/app/571430/Drone_Hero/)                              |Yes       |[Proton](https://www.protondb.com/app/571430)                                                   |
|[**Defendion**](https://store.steampowered.com/app/848710/Defendion/)                                |?         |No tests yet                                                                                    |
|[Bonfire](https://store.steampowered.com/app/1232310/Bonfire/)                                       |?         |No tests yet                                                                                    |
|[Cliffstone Manor](https://store.steampowered.com/app/698910/Cliffstone_Manor/)                      |Yes       |[Proton](https://www.protondb.com/app/698910)                                                   |
|[Moonshot Galaxy](https://store.steampowered.com/app/471160/Moonshot_Galaxy/)                        |Yes       |[Proton](https://www.protondb.com/app/471160)                                                   |
|[Protagon VR](https://store.steampowered.com/app/698100/Protagon_VR/)                                |?         |No tests yet                                                                                    |
|[Power Tools VR](https://store.steampowered.com/app/591920/Power_Tools_VR/)                          |?         |No tests yet                                                                                    |
|[Race The Sun](https://store.steampowered.com/app/253030/Race_The_Sun/)                              |Yes       |[Proton](https://www.protondb.com/app/253030)                                                   |
|[SpellPunk VR](https://store.steampowered.com/app/1258980/SpellPunk_VR/)                             |?         |No tests yet                                                                                    |
|[Strings](https://store.steampowered.com/app/1213810/Strings/)                                       |?         |No tests yet                                                                                    |
|[SweeperVR](https://store.steampowered.com/app/716360/SweeperVR/)                                    |?         |No tests yet                                                                                    |
|[PolyCube](https://store.steampowered.com/app/798790/PolyCube/)                                      |?         |No tests yet                                                                                    |
|[UNTITLED](https://store.steampowered.com/app/639770/UNTITLED/)                                      |?         |No tests yet                                                                                    |
|[Intruders: Hide and Seek](https://store.steampowered.com/app/1045840/Intruders_Hide_and_Seek/)      |--        |[Poor performance](https://www.protondb.com/app/1045840)                                        |

# Budget Price Tier Games

|**Game**                                                                                             |**Linux?**|**Details**                                                                                     |
|-----------------------------------------------------------------------------------------------------|----------|------------------------------------------------------------------------------------------------|
|[**Apex Construct**](https://store.steampowered.com/app/694090/Apex_Construct/)                      |?         |No tests yet                                                                                    |
|[**FORM**](https://store.steampowered.com/app/408520/FORM/)                                          |Yes       |[Proton](https://www.protondb.com/app/408520)                                                   |
|[**NIGHTSTAR: Alliance**](https://store.steampowered.com/app/864970/NIGHTSTAR_Alliance/)             |?         |No tests yet                                                                                    |
|[**Blasters of the Universe**](https://store.steampowered.com/app/490490/Blasters_of_the_Universe/)  |Yes       |[Proton](https://www.protondb.com/app/490490)                                                   |
|[**FREEDIVER: Triton Down**](https://store.steampowered.com/app/995230/FREEDIVER_Triton_Down/)       |Yes       |[Proton](https://www.protondb.com/app/995230)                                                   |
|[**Keep Talking and Nobody Explodes**](https://store.steampowered.com/app/341800/Keep_Talking_and_Nobody_Explodes/)|Yes       |[Proton](https://www.protondb.com/app/341800)                                                   |
|[**Orb Labs**](https://store.steampowered.com/app/843010/Orb_Labs_Inc/)                              |?         |No tests yet                                                                                    |
|[**Thumper**](https://store.steampowered.com/app/356400/Thumper/)                                    |Yes       |[Proton](https://www.protondb.com/app/356400)                                                   |
|[**Vetrix**](https://store.steampowered.com/app/1307070/Vetrix/)                                     |?         |No tests yet                                                                                    |
|[**Transpose**](https://store.steampowered.com/app/835950/Transpose/)                                |Yes       |[Proton](https://www.protondb.com/app/835950)                                                   |
|[Operation Warcade VR](https://store.steampowered.com/app/639270/Operation_Warcade_VR/)              |Yes       |[Proton](https://www.protondb.com/app/639270)                                                   |
|[Echo Grotto](https://store.steampowered.com/app/705870/Echo_Grotto/)                                |?         |No tests yet                                                                                    |
|[HATCHICK](https://store.steampowered.com/app/677190/HATCHICK/)                                      |?         |No tests yet                                                                                    |
|[Journey For Elysium](https://store.steampowered.com/app/1036260/Journey_For_Elysium/)               |?         |No tests yet                                                                                    |
|[Just In Time Incorporated](https://store.steampowered.com/app/592030/Just_In_Time_Incorporated/)    |Yes       |[Proton](https://www.protondb.com/app/592030)                                                   |
|[Psychonauts in the Rhombus of Ruin](https://store.steampowered.com/app/788690/Psychonauts_in_the_Rhombus_of_Ruin/)|Yes       |[Proton](https://www.protondb.com/app/788690)                                                   |
|[Shooty Skies Overdrive](https://store.steampowered.com/app/1300490/Shooty_Skies_Overdrive/)         |?         |No tests yet                                                                                    |
|[Portable Farm](https://store.steampowered.com/app/1434890/Portable_Farm/)                           |?         |No tests yet                                                                                    |
|[Balloonatics](https://store.steampowered.com/app/744600/Balloonatics/)                              |Yes       |Native                                                                                          |
|[Axegend VR](https://store.steampowered.com/app/1153460/Axegend_VR/)                                 |?         |No tests yet                                                                                    |
|[Buzludzha VR](https://store.steampowered.com/app/1064900/Buzludzha_VR/)                             |?         |No tests yet                                                                                    |
|[Carnival Games VR](https://store.steampowered.com/app/458920/Carnival_Games_VR/)                    |Yes       |[Proton](https://www.protondb.com/app/458920)                                                   |
|[Bandit Point](https://store.steampowered.com/app/1105430/Bandit_Point/)                             |?         |No tests yet                                                                                    |
|[Downward Spiral: Horus Station](https://store.steampowered.com/app/690620/Downward_Spiral_Horus_Station/)|?         |No tests yet                                                                                    |
|[Flotilla 2](https://store.steampowered.com/app/592100/Flotilla_2/)                                  |?         |No tests yet                                                                                    |
|[VR Furballs - Demolition](https://store.steampowered.com/app/704470/VR_Furballs__Demolition/)       |Yes       |[Proton](https://www.protondb.com/app/704470)                                                   |
|[Townsmen VR](https://store.steampowered.com/app/749960/Townsmen_VR/)                                |Yes       |[Proton](https://www.protondb.com/app/749960)                                                   |
|[Starblazer](https://store.steampowered.com/app/979520/Starblazer/)                                  |Yes       |[Proton](https://www.protondb.com/app/979520)                                                   |
|[Skyworld: Kingdom Brawl](https://store.steampowered.com/app/977690/Skyworld_Kingdom_Brawl/)         |?         |No tests yet                                                                                    |
|[RuneSage](https://store.steampowered.com/app/542030/RuneSage/)                                      |?         |No tests yet                                                                                    |
|[2945VR](https://store.steampowered.com/app/1351980/2945VR/)                                         |?         |No tests yet                                                                                    |

# Mid Price Tier

|**Game**                                                                                             |**Linux?**|**Details**                                                                                     |
|-----------------------------------------------------------------------------------------------------|----------|------------------------------------------------------------------------------------------------|
|[**Groundhog Day: Like Father Like Son**](https://store.steampowered.com/app/1087500/Groundhog_Day_Like_Father_Like_Son/)|--        |[ProtonDB](https://www.protondb.com/app/1087500)                                                |
|[**HORIZON VANGUARD**](https://store.steampowered.com/app/598740/HORIZON_VANGUARD/)                  |?         |No tests yet                                                                                    |
|[**Fujii**](https://store.steampowered.com/app/589040/Fujii/)                                        |Yes       |[Proton](https://www.protondb.com/app/589040)                                                   |
|[**Garden of the Sea**](https://store.steampowered.com/app/1086850/Garden_of_the_Sea/)               |Yes       |[Proton](https://www.protondb.com/app/1086850)                                                  |
|[**GORN**](https://store.steampowered.com/app/578620/GORN/)                                          |Yes       |[Proton](https://www.protondb.com/app/578620)                                                   |
|[**Contractors**](https://store.steampowered.com/app/963930/Contractors/)                            |--        |[ProtonDB](https://www.protondb.com/app/963930)                                                 |
|[**1976 - Back to midway**](https://store.steampowered.com/app/1118070/1976__Back_to_midway/)        |Yes       |[Proton](https://www.protondb.com/app/1118070)                                                  |
|[**Creed**](https://store.steampowered.com/app/804490/Creed_Rise_to_Glory/)                          |Yes       |[Proton](https://www.protondb.com/app/804490)                                                   |
|[**Bizarre Barber**](https://store.steampowered.com/app/1206910/Bizarre_Barber/)                     |?         |No tests yet                                                                                    |
|[**Prison Boss**](https://store.steampowered.com/app/673600/Prison_Boss_VR/)                         |?         |No tests yet                                                                                    |
|[**Tabletop Simulator**](https://store.steampowered.com/app/286160/Tabletop_Simulator/)              |--        |[ProtonDB](https://www.protondb.com/app/673600)                                                 |
|[Cubism](https://store.steampowered.com/app/804530/Cubism/)                                          |?         |No tests yet                                                                                    |
|[Gravity Lab](https://store.steampowered.com/app/408340/Gravity_Lab/)                                |?         |No tests yet                                                                                    |
|[**Westworld Awakening**](https://store.steampowered.com/app/1133320/Westworld_Awakening/)           |Yes       |[Proton](https://www.protondb.com/app/1133320)                                                  |
|[Garry's Mod](https://store.steampowered.com/app/4000/Garrys_Mod/)                                   |--        |[vrmod doesn’t support Linux](https://steamcommunity.com/sharedfiles/filedetails/?id=1678408548)|
|[Superfly](https://store.steampowered.com/app/1413020/Superfly/)                                     |?         |No tests yet                                                                                    |
|[**Ultrawings**](https://store.steampowered.com/app/639130/Ultrawings/)                              |Yes       |[Proton](https://www.protondb.com/app/639130)                                                   |
|[Shooty Fruity](https://store.steampowered.com/app/666100/Shooty_Fruity/)                            |Yes       |[Proton](https://www.protondb.com/app/666100)                                                   |
|[RUSH](https://store.steampowered.com/app/844040/RUSH/)                                              |Yes       |[Proton](https://www.protondb.com/app/844040)                                                   |
|[Climbey](https://store.steampowered.com/app/520010/Climbey/)                                        |Yes       |[Proton](https://www.protondb.com/app/520010)                                                   |
|[Space Pirate Trainer](https://store.steampowered.com/app/418650/Space_Pirate_Trainer/)              |Yes       |[Proton](https://www.protondb.com/app/418650)                                                   |
|[Spectro](https://store.steampowered.com/app/719520/Spectro/)                                        |Yes       |[Proton](https://www.protondb.com/app/719520)                                                   |
|[The Curious Tale of the Stolen Pets](https://store.steampowered.com/app/1099500/The_Curious_Tale_of_the_Stolen_Pets/)|Yes       |[Proton](https://www.protondb.com/app/1099500)                                                  |
|[ViSP - Virtual Space Port](https://store.steampowered.com/app/800290/ViSP__Virtual_Space_Port/)     |Yes       |[Proton](https://www.protondb.com/app/800290)                                                   |
|[A Fisherman's Tale](https://store.steampowered.com/app/559330/A_Fishermans_Tale/)                   |Yes       |[Proton](https://www.protondb.com/app/559330)                                                   |
|[I Expect You To Die](https://store.steampowered.com/app/587430/I_Expect_You_To_Die/)                |Yes       |[Proton](https://www.protondb.com/app/587430)                                                   |
|[Ultimate Fishing Simulator VR](https://store.steampowered.com/app/1024010/Ultimate_Fishing_Simulator_VR/)|?         |No tests yet                                                                                    |
|[The Forest](https://store.steampowered.com/app/242760/The_Forest/)                                  |?         |[Two VR tests too old](https://www.protondb.com/app/242760)                                     |
|[The Mage's Tale](https://store.steampowered.com/app/766320/The_Mages_Tale/)                         |--        |[ProtonDB](https://www.protondb.com/app/766320)                                                 |
|[2MD: VR Football](https://store.steampowered.com/app/674850/2MD_VR_Football/)                       |?         |No tests yet                                                                                    |
|[Deism](https://store.steampowered.com/app/525680/Deisim/)                                           |Yes       |[Proton](https://www.protondb.com/app/525680)                                                   |
|[Cave Digger](https://store.steampowered.com/app/844380/Cave_Digger_VR/)                             |Yes       |[Proton](https://www.protondb.com/app/844380)                                                   |
|[The Thrill of the Fight](https://store.steampowered.com/app/494150/The_Thrill_of_the_Fight__VR_Boxing/)|Yes       |[Proton](https://www.protondb.com/app/494150)                                                   |
|[Carly and the Reaperman](https://store.steampowered.com/app/547480/Carly_and_the_Reaperman__Escape_from_the_Underworld/)|?         |No tests yet                                                                                    |
|[Grapple Tournament](https://store.steampowered.com/app/1384320/Grapple_Tournament/)                 |?         |No tests yet                                                                                    |
|[Gun Club VR](https://store.steampowered.com/app/691320/Gun_Club_VR/)                                |Yes       |[Proton](https://www.protondb.com/app/691320)                                                   |
|[Swords of Gurrah](https://store.steampowered.com/app/833090/Swords_of_Gurrah/)                      |Yes       |[Proton](https://www.protondb.com/app/833090)                                                   |
|[Hellblade: Senua's Sacrifice](https://store.steampowered.com/app/414340/Hellblade_Senuas_Sacrifice/)|?         |No VR tests yet                                                                                 |
|[IL-2 Sturmovik: Battle of Stalingrad](https://store.steampowered.com/app/307960/IL2_Sturmovik_Battle_of_Stalingrad/)|Yes       |[Proton](https://www.protondb.com/app/307960)                                                   |
|[Marble Land](https://store.steampowered.com/app/503280/Marble_Land/)                                |?         |No tests yet                                                                                    |
|[Combat Tested](https://store.steampowered.com/app/750010/Combat_Tested/)                            |?         |No tests yet                                                                                    |
|[Blind](https://store.steampowered.com/app/406860/Blind/)                                            |Yes       |[Proton](https://www.protondb.com/app/406860)                                                   |
|[Adapt or Perish](https://store.steampowered.com/app/870730/Adapt_or_Perish/)                        |--         |Unity version used lacks VR support on Linux                                                   |
|[Pinball FX2 VR](https://store.steampowered.com/app/547590/Pinball_FX2_VR/)                          |--        |[ProtonDB](https://www.protondb.com/app/547590)                                                 |
|[Assetto Corsa](https://store.steampowered.com/app/347990/Assetto_Corsa__Dream_Pack_1/)              |Yes       |[Proton](https://www.protondb.com/app/244210) ([needs alt launcher](https://gitlab.com/vr-on-linux/VR-on-Linux/-/issues/16))|
|[Seeking Dawn](https://store.steampowered.com/app/859340/Seeking_Dawn/)                              |Yes       |[Proton](https://www.protondb.com/app/859340)                                                   |

# Mid-er Price Tier

|**Game**                                                                                             |**Linux?**|**Details**                                                                                     |
|-----------------------------------------------------------------------------------------------------|----------|------------------------------------------------------------------------------------------------|
|[**Pavlov VR**](https://store.steampowered.com/app/555160/Pavlov_VR/)                                |Yes       |[Proton](https://www.protondb.com/app/555160)                                                   |
|[**Vertigo Remastered**](https://store.steampowered.com/app/1318090/Vertigo_Remastered/)             |Yes       |[Proton](https://www.protondb.com/app/1318090)                                                  |
|[**Budget Cuts 2: Mission Insolvency**](https://store.steampowered.com/app/1092430/Budget_Cuts_2_Mission_Insolvency/)|Yes       |[Proton](https://www.protondb.com/app/1092430)                                                  |
|[**Superhot VR**](https://store.steampowered.com/app/617830/SUPERHOT_VR/)                            |Yes       |[Proton](https://www.protondb.com/app/617830)                                                   |
|[**Into the Radius**](https://store.steampowered.com/app/1012790/Into_the_Radius_VR/)                |Yes       |[Proton](https://www.protondb.com/app/1012790)                                                  |
|[**Mini Motor Racing X**](https://store.steampowered.com/app/1303990/Mini_Motor_Racing_X/)           |Yes       |[Proton](https://www.protondb.com/app/1303990)                                                  |
|[**Stride**](https://store.steampowered.com/app/1292040/STRIDE/)                                     |Yes       |[Proton](https://www.protondb.com/app/1292040)                                                  |
|[**BattleGroupVR**](https://store.steampowered.com/app/1178780/BattleGroupVR/)                       |Yes       |[Proton](https://www.protondb.com/app/1178780)                                                  |
|[**Until You Fall**](https://store.steampowered.com/app/858260/Until_You_Fall/)                      |--        |[ProtonDB](https://www.protondb.com/app/858260)                                                 |
|[**STAR WARS™: Squadrons**](https://store.steampowered.com/app/1222730/STAR_WARS_Squadrons/)         |Yes*      |[Proton](https://www.protondb.com/app/1222730) (*EasyAntiCheat=single only)                     |
|[**Yupitergrad**](https://store.steampowered.com/app/1352020/Yupitergrad/)                           |?         |No tests yet                                                                                    |
|[**Jet Island**](https://store.steampowered.com/app/587220/Jet_Island/)                              |Yes       |[Proton](https://www.protondb.com/app/587220)                                                   |
|[**Paper Beast**](https://store.steampowered.com/app/1232570/Paper_Beast/)                           |?         |No tests yet                                                                                    |
|[**Falcon Age**](https://store.steampowered.com/app/1075080/Falcon_Age/)                             |Yes       |[Proton](https://www.protondb.com/app/1075080)                                                  |
|[Gadgeteer](https://store.steampowered.com/app/746560/Gadgeteer/)                                    |?         |No tests yet                                                                                    |
|[Ironwolf VR](https://store.steampowered.com/app/552080/IronWolf_VR/)                                |Yes       |[Proton](https://www.protondb.com/app/552080)                                                   |
|[Hotel RnR](https://store.steampowered.com/app/1011290/Hotel_RnR/)                                   |?         |No tests yet                                                                                    |
|[Duck Season](https://store.steampowered.com/app/503580/Duck_Season/?curator_clanid=32973442)        |Yes       |[Proton](https://www.protondb.com/app/503580)                                                   |
|[Down the Rabbit Hole](https://store.steampowered.com/app/1215270/Down_the_Rabbit_Hole/)             |Yes       |[Proton](https://www.protondb.com/app/1215270)                                                  |
|[3dSen VR](https://store.steampowered.com/app/954280/3dSen_VR/)                                      |Yes       |[Proton](https://www.protondb.com/app/954280)                                                   |
|[Thief Simulator VR](https://store.steampowered.com/app/1019550/Thief_Simulator_VR/)                 |Yes       |[Proton](https://www.protondb.com/app/1019550)                                                  |
|[Vox Machinae](https://store.steampowered.com/app/334540/Vox_Machinae/)                              |Yes       |[Proton](https://www.protondb.com/app/334540)                                                   |
|[Windlands 2](https://store.steampowered.com/app/458580/Windlands_2/)                                |Yes       |[Proton](https://www.protondb.com/app/458580)                                                   |
|[Hello Puppets](https://store.steampowered.com/app/1426280/Hello_Puppets/)                           |Yes       |[Proton](https://www.protondb.com/app/1426280)                                                  |
|[Moss](https://store.steampowered.com/app/846470/Moss/)                                              |Yes       |[Proton](https://www.protondb.com/app/846470)                                                   |
|[Skytropolis](https://store.steampowered.com/app/629040/Skytropolis/)                                |?         |No tests yet                                                                                    |
|[Pixel Ripped 1995](https://store.steampowered.com/app/1178140/Pixel_Ripped_1995/)                   |?         |No tests yet                                                                                    |
|[ModBox](https://store.steampowered.com/app/414120/Modbox/)                                          |?         |No VR tests yet                                                                                 |
|[New Retro Arcade: Neon](https://store.steampowered.com/app/465780/New_Retro_Arcade_Neon/)           |?         |No VR tests yet                                                                                 |
|[Phasmophobia](https://store.steampowered.com/app/739630/Phasmophobia/)                              |Yes       |[Proton](https://www.protondb.com/app/739630) (No voice recognition)                            |
|[Synth Rider](https://store.steampowered.com/app/885000/Synth_Riders/)                               |Yes       |[Proton](https://www.protondb.com/app/885000)                                                   |

# Higher Price Tier

|**Game**                                                                                             |**Linux?**|**Details**                                                                                     |
|-----------------------------------------------------------------------------------------------------|----------|------------------------------------------------------------------------------------------------|
|[**Tales Of Glory**](https://store.steampowered.com/app/636970/Tales_Of_Glory/)                      Yes       |[Proton](https://www.protondb.com/app/636970)                                                   |
|[L.A. Noire: The VR Case Files](https://store.steampowered.com/app/722230/LA_Noire_The_VR_Case_Files/)|--        |[ProtonDB](https://www.protondb.com/app/722230)                                                 |
|[Universe Sandbox](https://store.steampowered.com/app/230290/Universe_Sandbox/)                      |?         |VR test too old                                                                                 |
|[The Talos Principle VR](https://store.steampowered.com/app/552440/The_Talos_Principle_VR/)          |Yes       |Native                                                                                          |
|[Skyrim VR](https://store.steampowered.com/app/611670/The_Elder_Scrolls_V_Skyrim_VR/)                |Yes       |[Proton](https://www.protondb.com/app/611670)                                                   |
|[Ragnarock](https://store.steampowered.com/app/1345820/Ragnarock/)                                   |Yes       |[Proton](https://www.protondb.com/app/1345820)                                                  |
|[Industrial Petting](https://store.steampowered.com/app/605450)                                      |Yes       |[Proton](https://www.protondb.com/app/676510)                                                   |
|[In Death](https://store.steampowered.com/app/605450/In_Death/)                                      |Yes       |[Proton](https://www.protondb.com/app/605450)                                                   |
|[Angry Birds VR: Isle of Pigs](https://store.steampowered.com/app/1001140/Angry_Birds_VR_Isle_of_Pigs/)|Yes       |[Proton](https://www.protondb.com/app/1001140)                                                  |
|[Arizona Sunshine](https://store.steampowered.com/app/342180/Arizona_Sunshine/)                      |Yes       |[Proton](https://www.protondb.com/app/342180)                                                   |
|[Automata Break](https://store.steampowered.com/app/1322010/Automata_Break/)                         |Yes       |[Proton](https://www.protondb.com/app/342180)                                                   |

# Top Price Tier

|**Game**                                                                                             |**Linux?**|**Details**                                                                                     |
|-----------------------------------------------------------------------------------------------------|----------|------------------------------------------------------------------------------------------------|
|[**Half-Life: Alyx**](https://store.steampowered.com/app/546560/HalfLife_Alyx/)                      Yes       |Native                                                                                          |
|[**Boneworks**](https://store.steampowered.com/app/823500/BONEWORKS/)                                |Yes       |[Proton](https://www.protondb.com/app/823500)                                                   |
|[**The Walking Dead: Saints & Sinners**](https://store.steampowered.com/app/916840/The_Walking_Dead_Saints__Sinners/)|Yes       |[Proton](https://www.protondb.com/app/916840)                                                   |
|[**Vacation Simulator**](https://store.steampowered.com/app/726830/Vacation_Simulator/)              |Yes       |[Proton](https://www.protondb.com/app/726830)                                                   |
|[**No Man's Sky**](https://store.steampowered.com/app/275850/No_Mans_Sky/)                           |Yes       |[Proton](https://www.protondb.com/app/1001140)* ([Don’t use experimental branch](https://gitlab.com/vr-on-linux/VR-on-Linux/-/issues/15))|
|[**Hot Dogs, Horseshoes, and Hand Grenades**](https://store.steampowered.com/app/450540/Hot_Dogs_Horseshoes__Hand_Grenades/)|Yes       |[Proton](https://www.protondb.com/app/450540)                                                   |
|[Pistol Whip](https://store.steampowered.com/app/1079800/Pistol_Whip/)                               |Yes       |[Proton](https://www.protondb.com/app/1079800)                                                  |
|[VTOL VR](https://store.steampowered.com/app/667970/VTOL_VR/)                                        |Yes       |[Proton](https://www.protondb.com/app/667970)                                                   |
|[AGOS - A Game Of Space](https://store.steampowered.com/app/1392700/AGOS__A_Game_Of_Space/)          |Yes       |[Proton](https://www.protondb.com/app/1392700)                                                  |
|[Sairento](https://store.steampowered.com/app/555880/Sairento_VR/)                                   |Yes       |[Proton](https://www.protondb.com/app/555880)                                                   |
|[Gnomes & Goblins](https://store.steampowered.com/app/1157940/Gnomes__Goblins/)                      |?         |No tests yet                                                                                    |
|[Disassembly VR](https://store.steampowered.com/app/973700/Disassembly_VR/)                          |?         |No tests yet                                                                                    |
